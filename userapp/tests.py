from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APIClient

class SignupAPITest(APITestCase):
    def setUp(self):
        self.client = APIClient()

    def test_api_can_create_a_new_user(self):
        # here creating a new user
        new_client = APIClient()
        response = new_client.post('/user/signup/',{"first_name": "soumen","last_name":"khatua","country_code":"+91",
            "phone_number":"9090909090","password":"soumen1234","confirm_password":"soumen1234"},format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
