from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver


#app
from .managers import UserManager

# drf level imports
from rest_framework.authtoken.models import Token


class User(AbstractBaseUser, PermissionsMixin):
    """
    Custom user model where phone_number is the unique identifier
    for authentication instead of usernames. This will later
    be linked to a Profile.
    """
    phone_regex = RegexValidator(regex = r'^\d{10}$', 
    	message="Phone number must be entered in the format: '9999999999'. Up to 10 digits allowed.")

    phone_extension =  RegexValidator(regex = r'^[+]\d{2}$', 
    	message = "Country Code must be in the format: '+91'. Up to 3 characters are allowed.")

    user_id = models.AutoField(primary_key = True,db_index = True)
    country_code = models.CharField(validators = [phone_extension],  max_length=10)
    phone_number = models.CharField(validators = [phone_regex], db_index=True, max_length=10, unique=True)
    first_name = models.CharField(max_length = 20)
    last_name = models.CharField(max_length = 20)
    is_staff = models.BooleanField(default = False)
    is_active = models.BooleanField(default = True)


    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    USERNAME_FIELD = 'phone_number'
    REQUIRED_FIELDS = []

    objects = UserManager()


# to create token for each new user by using django signal
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user = instance)

