## Users Conversations assignment, here I'm using Token Authentication and default sqlite3 database

### create one  virtual enviroment and run this command
```
pip install -r requirements.txt
```
### Run these Following command to make the database schema
```
python manage.py makemigrations
python manage.py migrate
```
### To run the project type this  below command
```
python manage.py runserver
```
### To create a new user follow this url and pass these below paramter
1. Password must contain at least 8 characters. 
2. Both password should be same
3. Phone Number should be unique

* http://127.0.0.1:8000/user/signup/
#### request parameter
```
{
    "first_name":"James",
    "last_name":"Bond",
    "country_code":"+91",
    "phone_number":"8888888888",
    "password":"@xyz123",
    "confirm_password":"@xyz123"
}
```
#### response parameter
```
{
    "message": "User created successfully.",
    "status": "Success",
    "token": "694adb254ba190c22d312fff6acf5cff025fe479"
}
```
### To get all saved users in your contacts type this url send a get request
* http://127.0.0.1:8000/contact/
#### response parameter
```
[
    {
        "user_id": 4,
        "country_code": "+99",
        "phone_number": "7777777777",
        "first_name": "Cristiano",
        "last_name": "Ronaldo"
    },
    {
        "user_id": 3,
        "country_code": "+91",
        "phone_number": "8888888888",
        "first_name": "James",
        "last_name": "Bond"
    }
    .........
    .........
]
```

### To save a new user in your contact type this url send a post request
* http://127.0.0.1:8000/contact/
### request parameter
```
{
	"phone_number":"users phone number(eg.7777777777)"
}
```
#### response parameter
```
{
    "message": "success"
}
```

### To show individual chat of a prticular user then type this url and send a get request
* http://127.0.0.1:8000/chat/<user_id>/
#### response parameter
```
[
    {
        "sender_details": {
            "user_id": 1,
            "phone_number": "8888888888",
            "first_name": "James",
            "last_name": "Bond"
        },
        "recevier_details": {
            "user_id": 2,
            "phone_number": "7777777778",
            "first_name": "Soumen",
            "last_name": "Khatua"
        },
        "text": "Hi,Soumen",
        "sent_time": "2020-11-03T09:31:30.119466Z"
    },
    {
        "sender_details": {
            "user_id": 2,
            "phone_number": "7777777779",
            "first_name": "Soumen",
            "last_name": "Khatua"
        },
        "recevier_details": {
            "user_id": 1,
            "phone_number": "8888888888",
            "first_name": "James",
            "last_name": "Bond"
        },
        "text": "Hi,Bond(007)",
        "sent_time": "2020-11-03T09:29:14.996452Z"
    }
],
.....
.....
```

### To send a message to a user then send a post request in this below url
* http://127.0.0.1:8000/send/message/
#### request parameter
```
{
	"user_id":1,
	"text":"What's in your mind?"
}
```
#### response parameter
```
{
    "message": "success"
}
```
