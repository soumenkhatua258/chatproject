# django level imports
from django.conf import settings

#  DRF levl imports
from rest_framework import serializers

# APP Level imports
from django.contrib.auth import get_user_model
from .models import UserContact, Conversation
from userapp.models import User




class UserDetailsSerializer(serializers.ModelSerializer):
	class Meta:
		model = get_user_model()
		fields = ('user_id','country_code','phone_number','first_name','last_name')


class  ConversationSerializer(serializers.ModelSerializer):
	sender_details = UserDetailsSerializer(source = 'sender')
	recevier_details = UserDetailsSerializer(source = 'recevier')
	class Meta:
		model = Conversation
		fields = ('sender_details','recevier_details','text','sent_time',)


class PostConversationSerializer(serializers.ModelSerializer):
	user_id = serializers.IntegerField()
	class Meta:
		model = Conversation
		fields = ('user_id','text')

	def create(self,validated_data):
		receving_user = User.objects.get(user_id = validated_data["user_id"])
		request = self.context.get("request")
		Conversation.objects.create(sender = request.user,recevier = receving_user,text = validated_data["text"])
		return validated_data


	def validate(self,attrs):
		user_check = User.objects.filter(user_id = attrs["user_id"])
		if not user_check.exists():
			message = "User Not Available,please invite."
			raise serializers.ValidationError({"message": message})
		return attrs
