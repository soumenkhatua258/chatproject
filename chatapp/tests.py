from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APIClient
from userapp.models import User
from rest_framework.authtoken.models import Token

class ChatUserListTestCase(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_superuser(first_name="Diya", last_name = 'Chakraborty',
                        country_code="+91",phone_number='4444444444',password = 'soumen123')
        self.token = Token.objects.get(user = self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)

    def test_get_all_contact_list(self):
        response = self.client.get("/contact/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
