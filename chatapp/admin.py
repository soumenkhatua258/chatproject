from django.contrib import admin
from .models import UserContact,Conversation


class UserContactAdmin(admin.ModelAdmin):
	list_display = ['main_user','my_contact','created_at']




class ConversationAdmin(admin.ModelAdmin):
	list_display = ['sender','recevier','text','sent_time','update_time']

admin.site.register(UserContact,UserContactAdmin)
admin.site.register(Conversation,ConversationAdmin)
