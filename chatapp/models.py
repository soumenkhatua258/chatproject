from django.db import models
from django.conf import settings


class UserContact(models.Model):
	main_user = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete = models.CASCADE,related_name = 'current_user')
	my_contact = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete = models.CASCADE,related_name = 'contact')
	created_at = models.DateTimeField(auto_now_add = True, null = True)
	updated_at = models.DateTimeField(auto_now = True, null = True)

	class Meta:
		unique_together = ("main_user", "my_contact")
		ordering  = ('-created_at',)


class Conversation(models.Model):
	sender = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.CASCADE, related_name = 'sending_user',null = True)
	recevier = models.ForeignKey(settings.AUTH_USER_MODEL ,on_delete = models.CASCADE, related_name = 'receving_user', null = True)
	text  = models.CharField(max_length = 250)
	sent_time = models.DateTimeField(auto_now_add = True, null = True)
	update_time = models.DateTimeField(auto_now = True, null = True)
	class Meta:
		verbose_name_plural = "Conversations"
