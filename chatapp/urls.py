from django.urls import path
from .views import ChatUserList,UserConversationAPIView, SendMessageAPIView

urlpatterns = [
    path('contact/', ChatUserList.as_view(), name='show_user_list'),
    path('chat/<int:user_id>/', UserConversationAPIView.as_view(), name='user_conversation'),
    path('send/message/', SendMessageAPIView.as_view(), name='send_message'),
]
