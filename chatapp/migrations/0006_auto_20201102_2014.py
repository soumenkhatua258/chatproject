# Generated by Django 3.1.2 on 2020-11-02 14:44

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('chatapp', '0005_conversation'),
    ]

    operations = [
        migrations.AlterField(
            model_name='conversation',
            name='recevier',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='receving_user', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='conversation',
            name='sender',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sending_user', to=settings.AUTH_USER_MODEL),
        ),
    ]
