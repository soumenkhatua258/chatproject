# django imports
from django.contrib.auth import login, authenticate
from django.db.models import F

# DRF Imports
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status

# APP Level imports
from userapp.models import User
from .models import UserContact, Conversation
from .serializers import  UserDetailsSerializer,ConversationSerializer,PostConversationSerializer


class ChatUserList(APIView):
	def get(self, request, *args, **kwargs):
		contacts = UserContact.objects.filter(main_user__user_id = request.user.user_id).values(
			user_id = F("my_contact__user_id"),country_code = F("my_contact__country_code"),
			phone_number = F("my_contact__phone_number"),
			first_name = F("my_contact__first_name"),last_name = F("my_contact__last_name"))
		serializer = UserDetailsSerializer(contacts, many = True)
		return Response(serializer.data, status = status.HTTP_200_OK)

	def post(self,request,*args,**kwargs):
		data  = request.data
		try:
			contact_details = User.objects.get(phone_number = data['phone_number'])

			UserContact.objects.create(main_user = request.user,my_contact = contact_details)
			message = 'success'
			return Response({'message': message},status = status.HTTP_201_CREATED)
		except Exception as e :
			message = "failure!!!"
			return Response({'message': message},status = status.HTTP_400_BAD_REQUEST)


class UserConversationAPIView(APIView):
	''' to get all chat message between two user'''
	def get(self,request,user_id,*args,**kwargs):
		try:
			recevied_user = User.objects.get(user_id = user_id)
			all_message = (
				Conversation.objects.select_related('sender','recevier').filter(sender = request.user, recevier = recevied_user) |
				Conversation.objects.select_related('sender','recevier').filter(sender = recevied_user, recevier = request.user)
			).order_by('-sent_time')
			serializer = ConversationSerializer(all_message, many = True)
		except User.DoesNotExist:
			message = "resource,not found!!!"
			return Response({'message': message}, status = status.HTTP_400_BAD_REQUEST)
		return Response(serializer.data,status = status.HTTP_200_OK)

class SendMessageAPIView(APIView):
	''' to send message of a particular user '''
	def post(self,request,*args,**kwargs):
		serializer = PostConversationSerializer(data = request.data,context={'request': request})
		if serializer.is_valid(raise_exception = True):
			serializer.save()
		message = 'success'
		return Response({'message': message}, status = status.HTTP_200_OK)
